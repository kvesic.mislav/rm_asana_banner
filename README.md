# RmAsanaBanner
This Chrome extension can remove Asana preminum banner. On page load it will 
20 times (once per second), try to find elements that have: 
**announcementBar yellow-announcement announcementBar--yellow visible** classes.
If this fails and the banner is visible the user can click the 
oranger and red A icon in the Chrome extensions bar.

## Instalation
```
$ git@gitlab.com:kvesic.mislav/rm_asana_banner.git
```
Open Chrome settings -> Extensions and turn Developer mode "ON".
Press "Load unpacked" and find the cloned folder.