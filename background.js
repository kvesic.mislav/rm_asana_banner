function run() {
  var tryNum = 0;
  var seconds = 20;

  function remove() {
    Array.from(document.getElementsByClassName("announcementBar yellow-announcement announcementBar--yellow visible")).forEach(
      element => {
        element.remove()
      });
  }
  setTimeout(function(){
    remove()
    tryNum++;
    if (tryNum < seconds) run()
  }, 1000);
}
chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete') {
    chrome.tabs.executeScript({
      code: '(' + run + ')();' 
    }, (results) => {});
  }
})
