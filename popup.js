function remove() {
  Array.from(document.getElementsByClassName("announcementBar yellow-announcement announcementBar--yellow visible")).forEach(
    element => {
      element.remove()
    });
}

//We have permission to access the activeTab, so we can call chrome.tabs.executeScript:
chrome.tabs.executeScript({
  //argument here is a string but function.toString() returns function's code
  code: '(' + remove + ')();' 
}, (results) => {});
